import { writeFileSync } from "fs";
import { RecurrenceRule, scheduleJob } from "node-schedule";
import { gzipSync } from "zlib";

const liveDataURL = "https://api.swu.de/mobility/v1/vehicle/trip/Trip";

const requestRecurrence = new RecurrenceRule();
requestRecurrence.second = 30;

scheduleJob(requestRecurrence, async () => {
    const response = await fetch(liveDataURL);
    const json = await response.json();
    const path = `data/${Date.now()}.json.gz`;
    writeFileSync(path, gzipSync(JSON.stringify(filterActiveVehicles(json))));
});

function filterActiveVehicles(json) {
    const filtered = json.VehicleTrip.TripData.filter(
        ({ IsActive }) => IsActive
    );
    return { VehicleTrip: { ...json.VehicleTrip, TripData: filtered } };
}
